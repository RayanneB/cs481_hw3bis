﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Tabbed
{
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async void OnTappedGoogle(object sender, EventArgs e)
        {
            await DisplayAlert("Info", "You chose Google", "OK");
        }


        private async void OnTappedFacebook(object sender, EventArgs e)
        {
            await DisplayAlert("Info", "You chose Facebook", "OK");
        }

        private async void OnTappedInstagram(object sender, EventArgs e)
        {
            await DisplayAlert("Info", "You chose Instagram", "OK");
        }

        private async void OnTappedLinkedin(object sender, EventArgs e)
        {
            await DisplayAlert("Info", "You chose LinkedIn", "OK");
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {

            await DisplayAlert("ALERT", "Exiting Application", "Ok");
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
    }
}
